package siddharth.com.tagtraqr;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rohan Tiwari on 08-10-2015.
 */
public class DeviceDBDataSource {
    SQLiteOpenHelper dbhelper;
    SQLiteDatabase database;
    private static final String LOGTAG = "DEVICES";

    private static final String[] allColumns = {
            DevicesDBOpenHelper.COLUMN_ID,
            DevicesDBOpenHelper.COLUMN_NAME,
            DevicesDBOpenHelper.COLUMN_MAC,
            DevicesDBOpenHelper.COLUMN_IMAGE,
            DevicesDBOpenHelper.COLUMN_LATI,
            DevicesDBOpenHelper.COLUMN_LONGI,
            DevicesDBOpenHelper.COLUMN_TIME,
            DevicesDBOpenHelper.COLUMN_TYPE,
    };

    public DeviceDBDataSource(Context context){

        dbhelper = new DevicesDBOpenHelper(context);
    }
    public void open() {
        Log.i(LOGTAG,"DataBase Open");
        database = dbhelper.getWritableDatabase();
    }
    public void close() {
        Log.i(LOGTAG,"DataBase Closed");
        dbhelper.close();
    }

    public Devices create(Devices device){
        ContentValues values = new ContentValues();
        values.put(DevicesDBOpenHelper.COLUMN_NAME,device.getName());
        values.put(DevicesDBOpenHelper.COLUMN_MAC, device.getMac());
        values.put(DevicesDBOpenHelper.COLUMN_IMAGE,device.getImage());
        values.put(DevicesDBOpenHelper.COLUMN_LATI,device.getLati());
        values.put(DevicesDBOpenHelper.COLUMN_LONGI, device.getLongi());
        values.put(DevicesDBOpenHelper.COLUMN_TIME,device.getTime());
        values.put(DevicesDBOpenHelper.COLUMN_TYPE,device.getType());

        long insertid = database.insert(DevicesDBOpenHelper.TABLE_DEVICES,null,values);


        device.setId(insertid);

        return device;
    }
    public void updateLocation(Devices device)
    {
        ContentValues cv = new ContentValues();
        cv.put("latitude",device.getLati());
        cv.put("longitude", device.getLongi());
        long result = database.update(DevicesDBOpenHelper.TABLE_DEVICES, cv, "devicesId=" + device.getId(), null);

    }

    public List<Devices> findAll(){
        List<Devices> tags = new ArrayList<Devices>();

        Cursor cursor = database.query(DevicesDBOpenHelper.TABLE_DEVICES, allColumns, null,null,null,null,null);

        Log.i(LOGTAG, " Returned " + cursor.getCount() + " rows");
        if (cursor.getCount() > 0)
        {
            while (cursor.moveToNext()) {
                Devices device = new Devices();
                device.setId(cursor.getLong(cursor.getColumnIndex(DevicesDBOpenHelper.COLUMN_ID)));
                device.setName(cursor.getString(cursor.getColumnIndex(DevicesDBOpenHelper.COLUMN_NAME)));
                device.setMac(cursor.getString(cursor.getColumnIndex(DevicesDBOpenHelper.COLUMN_MAC)));
                device.setLati(cursor.getDouble(cursor.getColumnIndex(DevicesDBOpenHelper.COLUMN_LATI)));
                device.setLongi(cursor.getDouble(cursor.getColumnIndex(DevicesDBOpenHelper.COLUMN_LONGI)));
                device.setTime(cursor.getString(cursor.getColumnIndex(DevicesDBOpenHelper.COLUMN_TIME)));
                device.setType(cursor.getString(cursor.getColumnIndex(DevicesDBOpenHelper.COLUMN_TYPE)));
                device.setImage(cursor.getBlob(cursor.getColumnIndex(DevicesDBOpenHelper.COLUMN_IMAGE)));
                tags.add(device);
            }
        }
        return tags;
    }

}
