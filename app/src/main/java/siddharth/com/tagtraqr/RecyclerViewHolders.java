package siddharth.com.tagtraqr;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;


public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener{

    public TextView objectName;
    public ImageView objectPhoto;
    public TextView objectAddress;
    private Switch mySwitch;
    public int position;
    DeleteItem mDeleteItem;
    final BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();


    public RecyclerViewHolders(final View itemView , DeleteItem deleteItem) {
        super(itemView);
        mDeleteItem = deleteItem;
        itemView.setOnLongClickListener(this);
        itemView.setOnClickListener(this);
        objectName = (TextView)itemView.findViewById(R.id.item_name);
        objectPhoto = (ImageView)itemView.findViewById(R.id.device_photo);
        objectAddress = (TextView)itemView.findViewById(R.id.object_address);
        mySwitch = (Switch) itemView.findViewById(R.id.switchStatus);

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    String macaddr=null;
                    macaddr=objectAddress.getText().toString();
                    Log.d("Mac Add Found", objectAddress.getText().toString());
                    Intent startSer = new Intent(itemView.getContext(), BleScanService.class);
                    startSer.putExtra("Mac", macaddr);
                    itemView.getContext().startService(startSer);
                    Intent startloc = new Intent(itemView.getContext(), LocationService.class);
                    startloc.putExtra("Mac", macaddr);
                    itemView.getContext().startService(startloc);

                } else {

                    Intent stopSer = new Intent(itemView.getContext(), BleScanService.class);
                    itemView.getContext().stopService(stopSer);
                    Log.d("yolo", "came herer");

                    Intent stopLoc = new Intent(itemView.getContext(), LocationService.class);
                    itemView.getContext().stopService(stopLoc);
                    Log.d("yolo", "came herer");

                }
            }
        });
    }
    @Override
    public void onClick(View view) {
//        String address = objectAddress.getText().toString();
        String item_name = objectName.getText().toString();
        Intent i = new Intent(view.getContext(), ScrollingActivity.class);
        i.putExtra("item_name",item_name);
        i.putExtra("mac_address", objectAddress.getText().toString());
        view.getContext().startActivity(i);

    }
    @Override
    public boolean onLongClick(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                itemView.getContext());
        // set title
        alertDialogBuilder.setTitle("Remove Bag");

        // set dialog message
        alertDialogBuilder
                .setMessage("Do you want to remove the selected bag ?")
                .setCancelable(false)
                .setPositiveButton("Remove",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        mDeleteItem.delete(getAdapterPosition());
//                        String name = objectName.getText().toString();
//                        String address = objectAddress.getText().toString();
                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        return false;
    }
}