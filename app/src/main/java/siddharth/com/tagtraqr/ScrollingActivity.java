package siddharth.com.tagtraqr;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class ScrollingActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    List<Double> Lati;
    List<Double> Longi;
    List<String> items;
    List<String> type;
    List<String> time;
    String Item_Name, mac_address;
    private TextView name, town,city,type1,time1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        name = (TextView)findViewById(R.id.device_name);
        town = (TextView)findViewById(R.id.town);
        city = (TextView)findViewById(R.id.city);
        type1 = (TextView)findViewById(R.id.type);
        time1 = (TextView)findViewById(R.id.time);

        Intent intent= getIntent();
        Item_Name=intent.getStringExtra("item_name");
        mac_address=intent.getStringExtra("mac_address");
        name.setText(Item_Name);
        type1.setText("yolo");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent i = new Intent(getApplicationContext(), NotificationShareActivity.class);
                i.putExtra("mac_address",mac_address);
                startActivity(i);
            }
        });

        addItemsToSpinner();


    }
    void addItemsToSpinner()
    {
        DevicesDBOpenHelper dataSource = new DevicesDBOpenHelper(getApplicationContext());
//        dataSource.open();
        items = dataSource.getAllNames();
        Lati = dataSource.getAllLatitudes();
        Longi = dataSource.getAllLongitudes();
        type = dataSource.getAllTypes();
        time = dataSource.getAllTime();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap=googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        int pos=items.indexOf(Item_Name);
        double la=Lati.get(pos);
        double lo=Longi.get(pos);
        type1.setText(type.get(pos));
        Log.d("time", time.get(pos));
        time1.setText(time.get(pos));
        CameraPosition googlePlex = CameraPosition.builder()
                .target(new LatLng(la,lo))
                .zoom(16)
                .bearing(0)
                .tilt(45)
                .build();
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(la,lo))
                .title(Item_Name)
                .snippet("Last seen at:"+time.get(pos)));
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses=geocoder.getFromLocation(la, lo, 1);
            town.setText(addresses.get(0).getAddressLine(0));
            city.setText(addresses.get(0).getAddressLine(1));
        } catch (IOException e) {
            e.printStackTrace();
        }
//        map.moveCamera(CameraUpdateFactory.newCameraPosition(googlePlex));
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 2000, null);
    }
}
